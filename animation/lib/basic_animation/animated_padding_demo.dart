import 'package:flutter/material.dart';

class AnimatedPaddingDemo extends StatefulWidget {
  AnimatedPaddingDemo({Key? key}) : super(key: key);

  @override
  _AnimatedPaddingDemoState createState() => _AnimatedPaddingDemoState();
}

class _AnimatedPaddingDemoState extends State<AnimatedPaddingDemo> {
  final initialPadding = 60.0;
  var padding;
  final duration = 800;
  bool started = false;
  @override
  void initState() {
    padding = initialPadding;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AnimatedPadding'),
        brightness: Brightness.dark,
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Container(
          width: 300.0,
          height: 300.0,
          child: AnimatedPadding(
            padding: EdgeInsets.all(padding),
            duration: Duration(milliseconds: duration),
            curve: Curves.easeInCubic,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Image.asset(
                  'images/heart.png',
                ),
                ClipOval(
                  child: Image.asset(
                    'images/zixia.png',
                    width: 60,
                    height: 60,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ],
            ),
            onEnd: () {
              if (started) {
                setState(() {
                  if (padding < initialPadding) {
                    padding = initialPadding;
                  } else {
                    padding -= 20.0;
                  }
                });
              }
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child:
            Icon(started ? Icons.stop : Icons.play_arrow, color: Colors.white),
        onPressed: () {
          setState(() {
            if (padding < initialPadding) {
              padding = initialPadding;
            } else {
              padding -= 20.0;
            }
            started = !started;
          });
        },
      ),
    );
  }
}
