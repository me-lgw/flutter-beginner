import 'dart:math';

import 'package:flutter/material.dart';

class ChineseWordAnimation extends StatefulWidget {
  const ChineseWordAnimation({Key? key}) : super(key: key);

  @override
  _ChineseWordAnimationState createState() => _ChineseWordAnimationState();
}

class _ChineseWordAnimationState extends State<ChineseWordAnimation>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;
  var currentIndex = 0;

  final words = [
    // 北
    [
      0x0000,
      0x0000,
      0x0660,
      0x0660,
      0x6660,
      0x7E7E,
      0x0E60,
      0x0660,
      0x0660,
      0x4670,
      0x467E,
      0xFE64,
      0x7E60,
      0x0000,
      0x0000,
      0x0000
    ],
    // 京
    [
      0x0000,
      0x0080,
      0x0180,
      0x7FFE,
      0x0800,
      0x3FF8,
      0x1818,
      0x1818,
      0x1FF8,
      0x0190,
      0x39F8,
      0x719C,
      0x21E4,
      0x0080,
      0x0000,
      0x0000,
    ],
// 欢
    [
      0x0000,
      0x0000,
      0x0720,
      0x237E,
      0xFF30,
      0x61B2,
      0x3CBC,
      0x0CD8,
      0x0C78,
      0x1E6C,
      0x3E66,
      0xF302,
      0x60C0,
      0x0000,
      0x0000,
      0x0000
    ],
// 迎
    [
      0x0000,
      0x23C0,
      0x7FCC,
      0x6CD8,
      0x6CC0,
      0x6CC0,
      0x6CFE,
      0x6FD8,
      0x7DF8,
      0x0C58,
      0x0018,
      0x003C,
      0xFFE6,
      0x0000,
      0x0000,
      0x0000
    ],
// 你
    [
      0x0000,
      0x0010,
      0x03B0,
      0x21B8,
      0xFF98,
      0x66DC,
      0x367C,
      0x179E,
      0x379A,
      0x66D8,
      0x6658,
      0x0638,
      0x0798,
      0x0000,
      0x0000,
      0x0000
    ],
  ];

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(controller);

    controller.repeat();
    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        // setState(() {
        //   currentIndex = (currentIndex + 1) % words.length;
        //   controller.reset();
        //   controller.forward();
        // });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<int> word = words[currentIndex];
    return Scaffold(
        appBar: AppBar(
          title: Text('汉字点阵'),
        ),
        body: Center(
          child: GridView.count(
            padding: EdgeInsets.all(20),
            crossAxisCount: 16,
            childAspectRatio: 1.0,
            crossAxisSpacing: 2.0,
            mainAxisSpacing: 2.0,
            children: List.generate(
              256,
              (index) {
                var wordIndex = index ~/ 16;
                return AnimatedWordSquare(
                  animation: animation,
                  raised: ((0x01 << (index % 16)) & word[wordIndex]) != 0,
                );
              },
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
            child: Text(
              'Stop',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            onPressed: () {
              controller.stop();
            }));
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedWordSquare extends AnimatedWidget {
  final raised;
  const AnimatedWordSquare(
      {Key? key, required Animation<double> animation, this.raised = false})
      : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;

    if (raised) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(2),
        child: Container(
          // transform: Matrix4.skewY(pi / 8)..setEntry(0, 1, 0.05),
          // transformAlignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.red[500],
            boxShadow: [
              BoxShadow(
                  color: Colors.orange,
                  offset: Offset(2, 5),
                  blurRadius: 1.0,
                  spreadRadius: 1),
            ],
          ),
        ),
      );
    } else {
      return ClipRRect(
        borderRadius: BorderRadius.circular(2),
        child: Container(
          // transform: Matrix4.skewY(0.3)..setEntry(0, 1, 0.005),
          // transformAlignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.grey[200],
            boxShadow: [
              // BoxShadow(
              //     color: Colors.black,
              //     offset: Offset(2, 0.5),
              //     blurRadius: 2.0,
              //     spreadRadius: 1),
            ],
          ),
        ),
      );
    }
  }
}
