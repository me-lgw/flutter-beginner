import 'package:flutter/material.dart';
import 'animated_text/animated_text_entry.dart';
import 'basic_animation/animation_homepage.dart';
import 'flipper/back_page.dart';
import 'flipper/front_page.dart';
import 'flipper/page_flipper.dart';
import 'moon_box/pack_box.dart';

void main() {
  runApp(MyApp());
}

// PageFlipper：卡片翻页动画
// PackBox：打包动画
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '动画',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const AnimationHomePage(),
    );
  }
}
