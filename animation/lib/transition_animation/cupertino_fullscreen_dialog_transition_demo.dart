import 'package:animation/components/primary_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CupertinoFullscreenDialogTransitionDemo extends StatefulWidget {
  CupertinoFullscreenDialogTransitionDemo({Key? key}) : super(key: key);

  @override
  _CupertinoFullscreenDialogTransitionDemoState createState() =>
      _CupertinoFullscreenDialogTransitionDemoState();
}

class _CupertinoFullscreenDialogTransitionDemoState
    extends State<CupertinoFullscreenDialogTransitionDemo>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller =
      AnimationController(duration: const Duration(seconds: 1), vsync: this);

  // //使用自定义曲线动画过渡效果
  late Animation<double> _animation1 =
      Tween<double>(begin: 0.0, end: 1.0).animate(_controller);
  late Animation<double> _animationZero =
      Tween<double>(begin: 0.0, end: 0.0).animate(_controller);

  late Animation<double> _animation2 =
      Tween<double>(begin: 0.0, end: 1.0).animate(_controller);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CupertinoFullscreenDialogTransition'),
        brightness: Brightness.dark,
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Column(
          children: [
            CupertinoFullscreenDialogTransition(
              primaryRouteAnimation: _animation2,
              secondaryRouteAnimation: _animationZero,
              child: Container(color: Colors.green, height: 300),
              linearTransition: true,
            ),
            Expanded(
              child: Container(),
            ),
            PrimaryButton(
              onPressed: () {
                _controller.forward();
              },
              title: '显示',
            ),
            PrimaryButton(
              onPressed: () {
                _controller.reverse();
              },
              title: '关闭',
            ),
            CupertinoFullscreenDialogTransition(
              primaryRouteAnimation: _animation1,
              secondaryRouteAnimation: _animationZero,
              child: Container(color: Colors.red, height: 300),
              linearTransition: true,
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.stop();
    _controller.dispose();
    super.dispose();
  }
}
