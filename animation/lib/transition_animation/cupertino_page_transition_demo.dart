import 'package:animation/components/primary_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CupertinoPageTransitionDemo extends StatefulWidget {
  CupertinoPageTransitionDemo({Key? key}) : super(key: key);

  @override
  _CupertinoPageTransitionDemoState createState() =>
      _CupertinoPageTransitionDemoState();
}

class _CupertinoPageTransitionDemoState
    extends State<CupertinoPageTransitionDemo>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller = AnimationController(
      duration: const Duration(milliseconds: 500), vsync: this);

  // //使用自定义曲线动画过渡效果
  late Animation<double> _animation1 =
      Tween<double>(begin: 0.0, end: 1.0).animate(_controller);
  late Animation<double> _animationZero =
      Tween<double>(begin: 0.0, end: 0.0).animate(_controller);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CupertinoPageTransition'),
        brightness: Brightness.dark,
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Row(
          children: [
            Expanded(
              child: Container(),
            ),
            CupertinoPageTransition(
              primaryRouteAnimation: _animation1,
              secondaryRouteAnimation: _animationZero,
              child: Container(
                  color: Colors.red,
                  width: MediaQuery.of(context).size.width / 2),
              linearTransition: false,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_arrow),
        onPressed: () {
          setState(() {
            if (_controller.isCompleted) {
              _controller.reverse();
            } else {
              _controller.forward();
            }
          });
        },
      ),
    );
  }

  @override
  void dispose() {
    _controller.stop();
    _controller.dispose();
    super.dispose();
  }
}
