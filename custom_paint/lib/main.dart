import 'package:flutter/material.dart';

import 'basic_paint/basic_paint.dart';
import 'basic_paint/curves_paint.dart';
import 'basic_paint/shapes_demo.dart';
import 'basic_paint/special_shapes_paint.dart';
import 'bezier_curve/basic_bezier_curve.dart';
import 'bezier_curve/bezier_animation_demo.dart';
import 'bezier_curve/gesture_bezier.dart';
import 'bezier_curve/interactive_gesture_bezier.dart';
import 'middle_autumn/draw_mooen.dart';
import 'middle_autumn/draw_sea.dart';
import 'middle_autumn/sea_moon.dart';
import 'painter/gradient_bound_demo.dart';
import 'painter/gradient_background_bubble.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: Brightness.dark,
      ),
      home: GradientBoundDemo(),
    );
  }
}
