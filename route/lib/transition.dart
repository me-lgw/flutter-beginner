import 'package:flutter/material.dart';

class TransitionPage extends StatelessWidget {
  const TransitionPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('转场演示'),
        brightness: Brightness.dark,
      ),
      body: Center(
        child: Container(
          color: Colors.blue[300],
        ),
      ),
    );
  }
}
