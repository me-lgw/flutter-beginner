import 'package:flutter/material.dart';
import 'package:home_framework/states/face_emotion.dart';

class StatelessNoDepend extends StatelessWidget {
  final FaceEmotion face;
  StatelessNoDepend({this.face, key}) : super(key: key) {
    print('constructor: 不依赖状态的组件');
  }

  @override
  Widget build(BuildContext context) {
    print('build：不依赖于状态的组件');
    return Center(
      child: Text('这是不依赖于状态的组件'),
    );
  }
}
