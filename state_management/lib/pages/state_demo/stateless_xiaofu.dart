import 'package:flutter/material.dart';
import 'package:home_framework/states/face_emotion.dart';

class StatelessXiaofu extends StatelessWidget {
  final ValueChanged<FaceEmotion> updateEmotion;
  final FaceEmotion face;
  StatelessXiaofu({@required this.updateEmotion, @required this.face, Key key})
      : assert(updateEmotion != null),
        super(key: key) {
    print('setState=====constructor：小芙');
  }

  @override
  Widget build(BuildContext context) {
    print('setState=====build：小芙');
    return Center(
      child: Column(children: [
        Text('小芙的表情：${face.emotion}'),
        TextButton(
            onPressed: () {
              updateEmotion(FaceEmotion(emotion: '高兴'));
            },
            child: Text('小芙表情变了')),
      ]),
    );
  }
}
