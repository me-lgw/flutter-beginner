import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:home_framework/pages/home/app.dart';
import 'package:home_framework/pages/dynamic/dynamic_detail.dart';
import 'package:home_framework/pages/dynamic/dynamic_edit.dart';
import 'package:home_framework/pages/state_demo/demo_set_state.dart';
import 'package:home_framework/pages/state_demo/goods_list.dart';
import 'package:home_framework/pages/state_demo/my_cart.dart';
import 'package:home_framework/pages/state_demo/state_controller_v0.dart';
import 'package:home_framework/pages/state_demo/state_controller_v1.dart';
import 'package:home_framework/pages/state_demo/state_controller_v2.dart';
import 'package:home_framework/pages/state_demo/stateful_stateless_demo.dart';

import 'package:home_framework/pages/user/login.dart';
import 'package:home_framework/pages/common/not_found.dart';
import 'package:home_framework/pages/common/permission_denied.dart';
import 'package:home_framework/routers/permission_router.dart';
import 'package:home_framework/pages/common/splash.dart';
import 'package:home_framework/pages/common/transition.dart';

import '../pages/dynamic/dynamic_add.dart';

class RouterManager {
  static String splashPath = '/';
  static String loginPath = '/login';
  static String homePath = '/home';
  static String dynamicPath = '/dynamic';
  static String dynamicEditPath = '$dynamicPath/edit/:id';
  static String dynamicAddPath = '$dynamicPath/add';
  static String dynamicDetailPath = '$dynamicPath/:id';
  static String transitionPath = '/transition';
  static String datingPath = '/dating';
  static String permissionDeniedPath = '/403';
  static String stateDemoPath = '/state';
  static String stateDemoPath1 = '/state1';
  static String cartPath = '/cart';
  static String goodsPath = '/goods';
  static String modelV0Path = '/model_v0';
  static String modelV1Path = '/model_v1';
  static String modelV2Path = '/model_v2';

  static PermissionRouter router;

  static void initRouter({List<String> whiteList}) {
    if (router == null) {
      router = PermissionRouter();
      defineRoutes(whiteList: whiteList);
    }
  }

  static final routeTable = {
    loginPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return LoginPage();
    }),
    dynamicEditPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return DynamicEditWrapper(params['id'][0]);
    }),
    dynamicAddPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return DynamicAddWrapper();
    }),
    dynamicDetailPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return DynamicDetailPage(params['id'][0]);
    }),
    splashPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return Splash();
    }),
    transitionPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return TransitionPage();
    }),
    homePath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return AppHomePage();
    }),
    stateDemoPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return StatefulStatelessDemoPage();
    }),
    stateDemoPath1: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return SetStateDemo();
    }),
    goodsPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return GoodsListPage();
    }),
    cartPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return MyCart();
    }),
    modelV0Path: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return StateViewController();
    }),
    modelV1Path: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return StateViewControllerV1();
    }),
    modelV2Path: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return StateViewControllerV2();
    }),
    permissionDeniedPath: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return PermissionDenied();
    }),
  };

  static void defineRoutes({List<String> whiteList}) {
    router.whiteList = whiteList;
    router.permissionDeniedPath = permissionDeniedPath;
    routeTable.forEach((path, handler) {
      router.define(path, handler: handler);
    });

    router.notFoundHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return NotFound();
    });
  }
}
