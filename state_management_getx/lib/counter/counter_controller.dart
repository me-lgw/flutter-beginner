import 'package:flutter/material.dart';

import 'package:get/get.dart';

class CounterController extends GetxController {
  int _counter = 0;

  static CounterController get to => Get.find();

  get counter => _counter;

  void increment() {
    _counter++;
    update();
  }
}

class CounterPage extends StatelessWidget {
  const CounterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('GetX计数器'),
      ),
      body: Center(
        child: GetBuilder<CounterController>(
          init: CounterController(),
          builder: (_) => Text(
            '${CounterController.to.counter}',
            style: TextStyle(
              color: Colors.blue,
              fontSize: 24.0,
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          CounterController.to.increment();
        },
      ),
    );
  }
}
