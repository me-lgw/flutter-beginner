import 'package:flutter/material.dart';

import 'package:get/get.dart';

class UniqueIdCounterController extends GetxController {
  int _counter = 0;

  static UniqueIdCounterController get to => Get.find();

  get counter => _counter;

  void increment() {
    _counter++;
    if (_counter % 2 == 0) {
      update(['even']);
    } else {
      update(['odd']);
    }
  }
}

class UniqueIdCounterPage extends StatelessWidget {
  UniqueIdCounterPage({Key? key}) : super(key: key);
  final UniqueIdCounterController idCounterController =
      UniqueIdCounterController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('GetX计数器'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GetBuilder<UniqueIdCounterController>(
              id: 'even',
              init: idCounterController,
              builder: (_) => Text(
                '${UniqueIdCounterController.to.counter}',
                style: TextStyle(
                  color: Colors.blue,
                  fontSize: 24.0,
                ),
              ),
            ),
            GetBuilder<UniqueIdCounterController>(
              id: 'odd',
              init: idCounterController,
              builder: (_) => Text(
                '${UniqueIdCounterController.to.counter}',
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 24.0,
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          UniqueIdCounterController.to.increment();
        },
      ),
    );
  }
}
