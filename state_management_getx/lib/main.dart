import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:state_management_getx/api/lottery_service_impl.dart';

import 'api/lottery_service.dart';
import 'api/lottery_service_mock.dart';
import 'counter/counter_controller.dart';
import 'counter/uid_counter_controller.dart';
import 'lottery/lottery.dart';
import 'getx_toolbox.dart';
import 'juejin/personal_homepage.dart';
import 'reactive_state/simple_reactive_demo.dart';
import 'state_mixins/personal_homepage_mixins.dart';
import 'traffic_light/traffic_led.dart';
import 'utils/global_dialogs.dart';
import 'workers/workers_demo.dart';

void main() {
  Get.lazyPut<LotteryService>(() => LotteryServiceMock());
  Get.lazyPut<PersonalMixinController>(
    () => PersonalMixinController(userId: '70787819648695'),
  );
  runApp(MyApp());
}

/// GetXDemo：GetX 常用工具演示
/// CounterPage：最简单的计数器状态管理示例
/// PersonalHomePage：掘金个人主页GetX 版本
/// UniqueIdCounterPage：根据 id 实现精准更新
/// TrafficLedPage：交通灯示例
/// MiddleAutumnLotteryPage：掘金抽奖揭秘
/// WokderDemoPage：Workers 示例
/// PersonalHomePageMixin：使用StateMixin
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      translations: Messages(),
      locale: Locale('zh', 'CN'),
      color: Colors.white,
      navigatorKey: Get.key,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: Brightness.light,
      ),
      home: PersonalHomePageMixin(),
      builder: GlobalDialogs.initDialogs(),
    );
  }
}
