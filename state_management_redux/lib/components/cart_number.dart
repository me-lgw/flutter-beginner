import 'package:flutter/material.dart';

class CartNumber extends StatelessWidget {
  final ValueChanged<int> onSub;
  final ValueChanged<int> onAdd;
  final int count;
  final double width;
  final double height;
  const CartNumber({
    Key? key,
    required this.count,
    required this.onAdd,
    required this.onSub,
    this.width = 40,
    this.height = 40,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double iconSize = 18;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(
            Icons.remove,
            color: Colors.red[400],
            size: iconSize,
          ),
          onPressed: () {
            onSub(count - 1);
          },
        ),
        Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: Colors.grey[200], borderRadius: BorderRadius.circular(4)),
          width: width,
          height: height,
          child: Text(count.toString()),
        ),
        IconButton(
          icon: Icon(
            Icons.add,
            color: Colors.blue[600],
            size: iconSize,
          ),
          onPressed: () {
            onAdd(count - 1);
          },
        ),
      ],
    );
  }
}
