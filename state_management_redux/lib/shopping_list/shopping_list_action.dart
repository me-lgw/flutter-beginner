import 'package:state_management_redux/shopping_list/shopping_item.dart';

class AddItemAction {
  final ShoppingItem item;

  AddItemAction({required this.item});
}

class ToggleItemStateAction {
  final ShoppingItem item;

  ToggleItemStateAction({required this.item});
}

class ReadOfflineAction {}

class ReadOfflineSuccessAction {
  List<ShoppingItem> items = [];

  ReadOfflineSuccessAction({required List<dynamic> offlineList}) {
    items =
        offlineList.map((jsonStr) => ShoppingItem.fromJson(jsonStr)).toList();
  }
}

class AddItemCountAction {
  final ShoppingItem item;

  AddItemCountAction({required this.item});
}

class SubItemCountAction {
  final ShoppingItem item;

  SubItemCountAction({required this.item});
}

class NoStateChangeAction {}
