import '../utils/http_util.dart';

class DynamicService {
  //static String host = 'http://localhost:3900/api/';
  static Future list(page, pageSize) async {
    var result = await HttpUtil.get(
      'dynamics',
      queryParams: {'page': page, 'pageSize': pageSize},
    );

    return result;
  }

  static Future get(String id) async {
    var result = await HttpUtil.get(
      'dynamics/' + id,
    );

    return result;
  }

  static Future post(Map<String, dynamic> data) async {
    var result = await HttpUtil.post('dynamics', data: data);

    return result;
  }

  static Future updateAll(String id, Map<String, dynamic> data) async {
    var result = await HttpUtil.put('dynamics/' + id, data: data);

    return result;
  }

  static Future update(String id, Map<String, dynamic> data) async {
    var result = await HttpUtil.patch('dynamics/' + id, data: data);

    return result;
  }

  static Future updateViewCount(String id) async {
    var result = await HttpUtil.patch('dynamics/view/' + id);

    return result;
  }

  static Future delete(String id) async {
    var result = await HttpUtil.delete(
      'dynamics/' + id,
    );

    return result;
  }
}
