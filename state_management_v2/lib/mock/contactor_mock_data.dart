class ContactorMockData {
  static Future<List<Map<String, dynamic>>> list(int page, int size) async {
    return List<Map<String, dynamic>>.generate(size, (index) {
      return {
        '_id': 'user${index + (page - 1) * size + 1}',
        'nickname': '岛上码农的第${index + (page - 1) * size + 1}个副本',
        'description': '这家伙很懒，什么都没留下',
        'avatar':
            'https://sf3-ttcdn-tos.pstatp.com/img/user-avatar/097899bbe5d10bceb750d5c69415518a~300x300.image',
      };
    });
  }
}
