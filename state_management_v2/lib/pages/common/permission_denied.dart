import 'package:flutter/material.dart';

class PermissionDenied extends StatelessWidget {
  const PermissionDenied({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('403'),
        brightness: Brightness.dark,
      ),
      body: Center(
        child: Text('403！抱歉，您无权限访问该功能'),
      ),
    );
  }
}
