import 'package:flutter/material.dart';
import 'package:home_framework/states/model_binding_v0.dart';
import 'package:home_framework/states/view_model.dart';

class StateViewController extends StatefulWidget {
  StateViewController({Key? key}) : super(key: key);

  @override
  _StateViewControllerState createState() => _StateViewControllerState();
}

class _StateViewControllerState extends State<StateViewController> {
  ViewModel currentModel = ViewModel();

  void _updateModel(ViewModel newModel) {
    if (newModel != currentModel) {
      setState(() {
        currentModel = newModel;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('模型绑定版本0'),
      ),
      body: Center(
        child: ModelBinding(
          model: currentModel,
          child: ViewController(_updateModel),
        ),
      ),
    );
  }
}

class ViewController extends StatelessWidget {
  final ValueChanged<ViewModel> updateModel;
  const ViewController(this.updateModel, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: Text('Hello World ${ViewModel.of(context).value}'),
      onPressed: () {
        updateModel(ViewModel(value: ViewModel.of(context).value + 1));
      },
    );
  }
}
