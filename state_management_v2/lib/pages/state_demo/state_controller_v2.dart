import 'package:flutter/material.dart';
import 'package:home_framework/states/model_binding_v2.dart';

class StateViewControllerV2 extends StatelessWidget {
  StateViewControllerV2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('模型绑定泛型版'),
      ),
      body: Center(
        child: ModelBindingV2(
          create: () => ViewModel(),
          child: ViewController(),
        ),
      ),
    );
  }
}

class ViewController extends StatelessWidget {
  const ViewController({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: Text('Hello World ${ModelBindingV2.of<ViewModel>(context).value}'),
      onPressed: () {
        ViewModel model = ModelBindingV2.of<ViewModel>(context);
        ModelBindingV2.update(context, ViewModel(value: model.value + 1));
      },
    );
  }
}
